{-# LANGUAGE BangPatterns        #-}
{-# LANGUAGE BlockArguments      #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE ScopedTypeVariables #-}

import Control.Monad.ST (runST)
import Data.Foldable (for_, traverse_)
import Data.Vector (Vector)
import GHC.Exts (toList)
import System.Environment (getArgs)
import System.Random (randomRIO)

import qualified Data.Vector         as Vec
import qualified Data.Vector.Mutable as MVec


main :: IO ()
main = wichteln . lines =<< readFile . head =<< getArgs

{- | Shuffle the given names and print them to a file in the following way:

   Name     of the file: Secret Santa
   Contents of the file: Person the Secret Santa as to gift cool stuff to

-}
wichteln :: [String] -> IO ()
wichteln names = traverse_ (uncurry writeFile) . getPartner =<< shuffle names

-- | For a list of the form @[x₁, x₂, ..., xₙ]@, create a list of partners of
-- the form @[(x₁, x₂), ..., (xₙ, x₁)]@.
getPartner :: [a] -> [(a, a)]
getPartner = \case
    ys@(x:xs) -> zip ys (xs ++ [x])
    _         -> []

-- | Shuffle a list in O(n) with sexy vectors and ST Monad goodness!
shuffle :: forall a. [a] -> IO [a]
shuffle xs =
    toList . go <$> traverse (\i -> randomRIO (i, n - 1)) [0 .. n - 2]
  where
    n :: Int
    n = length xs

    go :: [Int] -> Vector a
    go rands = runST do
        vec <- Vec.unsafeThaw $ Vec.fromList xs
        for_ (zip [0 ..] rands) \(i, j) -> do
            vi <- MVec.read vec i
            MVec.write vec i =<< MVec.read vec j
            MVec.write vec j vi
        Vec.unsafeFreeze vec
