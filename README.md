# Secret Santa
Very simple secret santa implementation in Haskell.  Takes a file with one name
per line as input (the people who want to play), generates files for all
participants with their name as the file name and the person they have to shower
with gifts as the contents of the file.
